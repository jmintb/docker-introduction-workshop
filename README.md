# Docker introduction workshop

## Install Docker
Install the latest version of Docker [here](https://docs.docker.com/install/).

**Verify your installation**

Run: `sudo docker run hello-world`

## Run mysql

We will start by having a look at mysql on [dockerhub](https://hub.docker.com/_/mysql).

**Start a mysql container.**

`sudo docker run --name my-database -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.7`

**List running containers.**

`sudo docker ps`

You should see the my-database container running.

**Run a command inside the container**

Docker provides the ability to run commands inside a running container. We can use this to administrate our database.

`sudo docker exec -it my-database bash`

Once inside the container we can connect to the database using the mysql cli.

`mysql -u root -p`

Remember the password we set when starting the container.

Type `exit` to exit the mysql cli and type `exit` again to exit the container bash session.

**Stop the container.**

The container will keep running until it is stopped.

`sudo docker stop my-database`

After stopping the container it still maintanes it state and can be started again with `sudo docker start my-database`.

**Remove the container.**

Once a container is no longer required and it has been stopped we can remove it use the rm command.
Remeber that all of the state will be gone after removing the container.

`sudo docker rm my-database`

**Start a mysql container with a volume.**

By default when we remove our container all it's state is gone, in otherwords container are stateless.
If we want to have persistent storage we need to attach a volume. A volume is storage managed by the host operating system.

`sudo docker run --name my-database -v my-database-volume:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.7`

**Exposing mysql to the host network.**

Containers are run with their own network that is not connected to the host network. 
If we want tools on the host operating system to connect to our database we have to expose a port from the container to the host.

`sudo docker run --name my-database -p 3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.7`

We can now connect to our database from the host.

`mysql -h localhost -P 3306 --protocol=tcp -u root -p`